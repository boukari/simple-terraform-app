variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}
variable "private_subnet_cidr_blocks" {
  default = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]
}
variable "public_subnet_cidr_blocks" {
  default = ["10.0.111.0/24", "10.0.112.0/24", "10.0.113.0/24"]
}

variable "k8s_version" {
  default = "1.27"
}

variable "k8s_cluster_name" {
  default = "bdi-simple-app-eks-cluster"
}

variable "aws_region" {
  default = "us-east-1"
}
