# Simple Terraform IAC

## Description

Ce code génère une infrastructure Terraform sur le cloud AWS

## Commandes utilitaires

```bash

# Initialisation du projet
terraform init

# Génère le plan de l'architechture
terraform plan

# Réalise l'architechture
terraform apply

# Détruit l'architechture
terraform destroy
```
